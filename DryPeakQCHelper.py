from paramiko import SSHClient
import paramiko
from IPython.display import display
from airtable import Airtable
import numpy as np
from matplotlib import pyplot as plt
import os

def connect_to_alto(output, server='192.168.1.211'):
    with output:
        client = SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(server,'22','alto','alto')
        display('Connected to Alto')        
        return client

def rosservicecall(cmd, client):
    base_cmd = '''export BUNDLE_CURRENT_PREFIX=/home/alto/alto_apps/output/dependencies/opt/ros/melodic 
        source /home/alto/alto_apps/output/dependencies/opt/ros/melodic/local_setup.bash 
        export COLCON_CURRENT_PREFIX=/home/alto/alto_apps/output/workspace/opt/built_workspace 
        source /home/alto/alto_apps/output/workspace/opt/built_workspace/local_setup.sh
    '''
    exec_cmd = base_cmd + cmd
    stdin, stdout, stderr = client.exec_command(exec_cmd,get_pty=True)        
    lines = stdout.readlines()
    display(lines)   
    return lines

def run_qc_test(client, cart_id):
    # Check that the SSH client is active and open transfer protocol
    ssh_status=client.get_transport().is_active()
    if ssh_status:
        print('SSH connection established')
    else:
        print ('SSH connection not established')    
    ftp = client.open_sftp()
    # Load the cartridge
    cmd = '''
        rosservice call /thermotor_controller/tm_load_cartridge
        '''
    lines = rosservicecall(cmd, client)  
    # Set the exposure time and write to file
    cmd = '''
        rosservice call /alto_optics/set_optimal_exposure_and_groups_req_srv "{auto_sequence_groups: false, exposure_min_threshold: 0, exposure_max_threshold: 1000}"
        '''
    lines = rosservicecall(cmd, client)  
    try:
        ftp.mkdir('/data/dry_cartridge_QC/%s' % (cart_id))
    except:
        pass
    with ftp.open('/data/dry_cartridge_QC/%s/ExpTimes.txt' % (cart_id), mode='wb') as exp_times:
        for line in lines:
            exp_times.write(line)
    # Capture the curves
    cmd = '''
        rosservice call /alto_optics/display_optical_signal_snapshot_req_srv \"{display_graphs: false, number_of_frames: 0, save_to_file_system: true, save_directory: '/data/dry_cartridge_QC/%s/'}\"
        ''' % (cart_id)
    lines = rosservicecall(cmd, client)     
    # Eject the cartridge
    cmd = '''
        rosservice call /thermotor_controller/tm_eject_cartridge
        '''
    lines = rosservicecall(cmd, client)  
    # Save the CartConf
    with ftp.open('/data/dry_cartridge_QC/%s/CartConf.txt' % (cart_id), mode='wb') as cart_conf:
        cart_conf.write('Cartridge %s\n' % (cart_id))
        cart_conf.write('Trial 1')  

def plot_curves(client, cart_id):
    fig, axs = plt.subplots(nrows=4,ncols=4,figsize=(48, 48))
    fig.subplots_adjust(hspace = .5, wspace=.001)
    axs = axs.ravel()
    # Check that the SSH client is active and open transfer protocol
    ssh_status=client.get_transport().is_active()
    if ssh_status:
        print('SSH connection established')
    else:
        print ('SSH connection not established')    
    ftp = client.open_sftp()
    folder = ''
    # Find the folder that has the curves
    for filename in ftp.listdir('//data/dry_cartridge_QC/%s' % (cart_id)):
        if not('BRIGHT' in filename) and not('DARK' in filename) and ('REFLECTANCE') in filename:
            folder = filename
            print(folder)
            break
    # Do the curve plotting
    for i in range (16):
        print('/data/dry_cartridge_QC/%s/%s/%s.csv' % (cart_id,folder, i+1))
        with ftp.open('/data/dry_cartridge_QC/%s/%s/%s.csv' % (cart_id,folder, i+1)) as curve_file:
            data = np.genfromtxt(curve_file,delimiter=',')
            label="Instrument "+str(cart_id)+", Test Loop "+str(1)
            axs[i].plot(data[:,0],data[:,1],label=label)
            axs[i].legend(loc='upper left')
            axs[i].set_title("Channel "+str(i+1))
            axs[i].set(xlabel="Wavelength (nm)", ylabel="Reflectance (Arb. U)")
    # Save the file
    title="Cartridge ID " + str(cart_id)
    fig.suptitle(title)
    filename=str(title)+'.png'
    plt.savefig(filename)
    plt.close()
    # Transfer file to server
    ftp.put(filename,'/data/dry_cartridge_QC/%s/%s' % (cart_id,filename))
    # Transfer file to google drive an delete from server
    try:
        os.mkdir('G:\Shared drives\Engineering\DMF\Sensors\Sensor-DFA-FFA OPQC\FFS Production QC Data\Cartridge Spectra QC\%s' % (cart_id))
    except:
        pass
    for f in ftp.listdir('/data/dry_cartridge_QC/%s' % (cart_id)):
        if(not '.' in f):
            os.mkdir('G:\Shared drives\Engineering\DMF\Sensors\Sensor-DFA-FFA OPQC\FFS Production QC Data\Cartridge Spectra QC\%s\%s' % (cart_id,f))
            for g in ftp.listdir('/data/dry_cartridge_QC/%s/%s' % (cart_id,f)):
                fl = open('G:\Shared drives\Engineering\DMF\Sensors\Sensor-DFA-FFA OPQC\FFS Production QC Data\Cartridge Spectra QC\%s\%s\%s' % (cart_id,f,g),'wb')
                ftp.getfo('/data/dry_cartridge_QC/%s/%s/%s' % (cart_id,f,g),fl)
                fl.close()
        else:
            fl = open('G:\Shared drives\Engineering\DMF\Sensors\Sensor-DFA-FFA OPQC\FFS Production QC Data\Cartridge Spectra QC\%s\%s' % (cart_id,f),'wb')
            ftp.getfo('/data/dry_cartridge_QC/%s/%s' % (cart_id,f),fl)
            fl.close()
    client.close()
